angular.module('starter').factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
}).service('myLogs', function(){
  this.list = [];

  this.add = function(event, data){
    console.log(event, data);
    this.list.unshift({event: event, data: JSON.stringify(data)});
    //if(this.list.length > 5)
    //  this.list.pop();
  };

  this.clear = function(){
    while (list.length > 0)
      list.pop();
  };
}).service('socket', function($rootScope, myLogs){
  this.client = null;

  this.connect = function () {
    var self = this;
    var server = 'https://sio.goabode.com';
    //var server = 'https://zigron.goabode.com';
    if (self.client) {
      myLogs.add("", 'Socket already connected.');
      return;
    }
    myLogs.add(server, 'Connecting Socket..');
    self.client = io.connect(server, {
      'forceNew': true,
      'reconnection': true,
      'transports' : ['websocket']
    });

    self.once('connect', function () {
      self.socketStatus = "connected";
      myLogs.add(server, 'Socket connected');

      self.once('com.goabode.gateway.mode', function (mode) {
        myLogs.add("com.goabode.gateway.mode", mode);
      });

      self.once('connect_error', function () {
        self.socketStatus = "connect_error";
        myLogs.add("", "Error in connecting socket!");
      });

      self.once('disconnect', function () {
        self.socketStatus = "disconnected";
        myLogs.add("", "Socket is Disconnected");
      });

      self.once('reconnect_attempt', function () {
        self.socketStatus = "reconnecting";
        myLogs.add("", "Socket is reconnecting..");
      });

      self.once('reconnect_failed', function () {
        self.socketStatus = "reconnection_error";
        myLogs.add("", "Socket reconnection failed..");
      });

      self.once('reconnect', function () {
        self.socketStatus = "reconnected";
        myLogs.add("", "Socket is reconnected!");
      });

      self.once('com.goabode.gateway.online', function (online) {
        myLogs.add('com.goabode.gateway.online', online);
      });

      self.once('com.goabode.device.update', function (deviceID) {
        myLogs.add("com.goabode.device.update", deviceID);
      });

      self.once('com.goabode.gateway.getrecord', function(obj){
        myLogs.add('com.goabode.gateway.getrecord', obj);
      });

      self.once('com.goabode.gateway.timeline', function (timelineObject) {
        myLogs.add("com.goabode.gateway.timeline", timelineObject);
      });

      self.once('com.goabode.gateway.timeline.update', function (timelineObject){
        myLogs.add('com.goabode.gateway.timeline.update', timelineObject);
      });
    });
  };

  this.once = function (eventName, callback) {
    if (!this.client) {
      myLogs.add(eventName, 'WARN: Socket not connected');
      return;
    }

    this.client.once(eventName, function () {
      myLogs.add(eventName, 'Socket once callback registered');
      var self = this;
      var args = arguments;
      $rootScope.$apply(function () {
        callback.apply(self.client, args);
      });
    });
  };

  this.on = function (eventName, callback) {
    if (!this.client) {
      myLogs.add(eventName, 'WARN: Socket not connected');
      return;
    }

    this.client.on(eventName, function () {
      myLogs.add(eventName, 'Socket callback registered');
      var self = this;
      var args = arguments;
      $rootScope.$apply(function () {
        callback.apply(self.client, args);
      });
    });
  };

  this.emit = function (eventName, data, callback) {
    if (!this.client) {
      myLogs.add(eventName, 'Socket not connected');
      return;
    }

    this.client.emit(eventName, data, function () {
      myLogs.add(eventName, 'Socket callback registered');
      var args = arguments;
      $rootScope.$apply(function () {
        if (callback) {
          callback.apply(this.client, args);
        }
      });
    });
  };

  this.disconnect = function () {
    if (this.client)
      this.client.disconnect();
    this.client = null;
  };
});
